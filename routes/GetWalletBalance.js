const express=require('express');
// var connect = require('../model/MySQLConnection');
var router = express.Router();
var connect = require('./connection');


router.route('/')
.get((req,res) => {

    var data=[req.headers.mobile_no];
    let sql = 'call GET_WALLET_BALANCE(?,@current_balance,@error_code,@error_desc)'
    let sql2='select @current_balance,@error_code,@error_desc';

    connect.connection()
    .then((conn) => {

        conn.query(sql,data,(error, results, fields) => {
           
            if(error){
                conn.release()
                console.log('Error:'+error.message);
                res.json({status:0})
            }
            console.log('results'+JSON.stringify(results))
            console.log('affectedRows'+results.affectedRows)
            if(results.affectedRows == 0)
                res.json({status:0})
            else{
                conn.query(sql2,(error,results,fields) => {
                    conn.release();
                    if(error){
                        console.log('Error',error.message);
                        res.json({status:0})
                    }
                    console.log('results',results)
                    console.log('current_balance',results[0]['@current_balance'])
                    res.json({status:1,details:results,current_balance:results[0]['@current_balance']})
    
                })
            }
        })

    })

})


// router.route('/cur_bal')
// .get((req,res) => {

//     var data=[req.headers.mobile_no];
//     let sql='select current_balance from mx_wallets where mobile_no = ?';

//     connect.connection()
//     .then((conn) => {

//         conn.query(sql,data,(error, results, fields) => {
//             if(error){
//                 console.log('Error:'+error.message);
//                 res.json({status:0})
//             }
       
//                 console.log('results',results)
//             if(results.length == 0){
//                 res.json({status:0})
//             }
//             else{
//                 if(results[0].current_balance == null)
//                     var current_balance=0
//                 res.json({status:1,current_balance:current_balance})
//             }
            

//         })

//     })

// })

module.exports = router;