var mysql = require('mysql');
var cryptify = require('./cryptify');


const express=require('express');
var connect = require('./connection');
var router = express.Router();
const config= require('../config')
const sgMail = require('@sendgrid/mail');

router.route('/savePassword')
    .post((req, res) => {
      console.log("Inside savePassword");
      var uname = req.body.userid;
      var password = req.body.password;
      password = cryptify.encrypt(password);
      var email=req.body.email;
      var origin_email=req.body.origin_email;
      console.log("EMAIL--")
      // Save password in database table
      //let sql = `UPDATE mx_wallets SET Password = ? WHERE Mobile_no = ?`;
      let sql = `UPDATE mx_wallets SET password = ? WHERE mobile_no = ?`;
      let data = [password, uname];

      connect.connection()
      .then((conn) => {
      conn.query(sql, data,(error, results, fields) => {
        conn.release();
        if (error){
          res.json({result:error.message});
        }
        else{
          const msg = {
            to: email,
            from: origin_email,
            html: '<p> </p>',
            templateId: config.SENDGRID_INFINITY_CASH_TEMPLATE,
        };
        sgMail.setApiKey(config.SENDGRID_KEY);
        sgMail.setSubstitutionWrappers('{{', '}}');
        sgMail.send(msg);
        console.log('mail sent')
          res.json({result:"password updated"});
        }
      });
      })
})


  router.route('/checkAuthenticity')
  .post((req, res) => {
      var uname = req.body.userid;
      var password = req.body.password;
      //console.log("uname in checkAuthenticity:",uname);
      
      //Check if particular user id available in database table
      //let sql = 'select Password from mx_wallets where Mobile_no='+uname;
      let sql = 'select password from mx_wallets where mobile_no='+uname;
      var validity = 0;
     connect.connection()
    .then((conn) => {
        
    conn.query(sql,  (err, results, fields) => {
      conn.release();
        if (err) {
            return console.error(err.message);
        }
        else{
          if(results.length == 0){
            //console.log("invalid user");
            res.json({result:"invalid user"});
          }
          else{
            dbPassword = results[0].password;
            dbPassword = cryptify.decrypt(dbPassword.toString());
            console.log("dbPassword:",dbPassword,",password:",password);
            if(password == dbPassword){
              res.json({result:"valid user"});
            }
            else{
              res.json({result:"invalid password"});
            }
          }
        }           

        });
    
    // connection.end();
   
    })
})


router.route('/verifyUser')
  .post((req, res) => {
      var uname = req.body.userid;
      //console.log("uname in checkAuthenticity:",uname);
      
      //Check if particular user id available in database table
      //let sql = 'select Password from mx_wallets where Mobile_no='+uname;
      let sql = 'select password,email from mx_wallets where mobile_no='+uname;
      var validity = 0;
     connect.connection()
    .then((conn) => {
        
    conn.query(sql,  (err, results, fields) => {
      conn.release();
        if (err) {
            return console.error(err.message);
        }
        else{
          console.log("results--"+JSON.stringify(results[0]))
          if(results.length == 0){
            console.log("invalid user");
            res.json({result:"invalid user"});
          }
          else{
            res.json({result:"valid user",email:results[0]['email']});
          }
        }           

        });
    
    // connection.end();
   
    })
})


  module.exports = router;
 
  