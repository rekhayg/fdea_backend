const express = require('express');
var con = require('./connection');
var router = express.Router();

router.route('/')
.post((req,res) => {

    var get_promotions = 'call GET_PROMOTIONAL_BANNERS(?,@banner1,@banner2,@banner3,@redirect1,@redirect2,@redirect3,@err_code,@err_desc)';
    var fetch_promotions = 'select @banner1,@banner2,@banner3,@redirect1,@redirect2,@redirect3,@err_code,@err_desc';
    var data = [req.body.mno];

    con.connection()
    .then((conn) => {

        conn.query(get_promotions,data,(err, result) => {
            if(err){
                conn.release();
                console.log("Error in getting the promotions:",err.message);
                res.json({status:-1})
            }
            else{
                conn.query(fetch_promotions,(err2,results) => {
                    conn.release();
                    if(err2){
                        console.log("Error in fetching the promotions:",err.message)
                        res.json({status:0})
                    }
                    else if(results[0]['@err_code'] == 'ERR'){
                        console.log("Error code:",results[0]['@err_code'],"\nError desc:",results[0]['@err_desc']);
                        console.log("Error while fetching the promotions. Check the erros logs from the database");
                        res.json({status:0});
                    }
                    else{
                        var banners = [];
                        if(results[0]['@banner1']){
                            var obj = {
                                banner: results[0]['@banner1'],
                                redirect: results[0]['@redirect1']
                            }
                            banners.push(obj);
                        }
                        if(results[0]['@banner2']){
                            var obj = {
                                banner: results[0]['@banner2'],
                                redirect: results[0]['@redirect2']
                            }
                            banners.push(obj);
                        }
                        if(results[0]['@banner3']){
                            var obj = {
                                banner: results[0]['@banner3'],
                                redirect: results[0]['@redirect3']
                            }
                            banners.push(obj);
                        }
                        console.log("banners:",banners);
                        res.json({status:1,
                                banners:banners})
                    }
                })
            }
        })

    })

})

module.exports = router;
