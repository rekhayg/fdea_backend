const express = require('express');
var connect = require('./connection');
var router = express.Router();

router.route('/')
.post((req,res) => {

    var uid = req.body.userid;
    console.log("uid:",uid);
    // var sql = "select * from mx_wallet_xns where Mobile_no="+uid;
    var data=[req.body.startLimit,req.body.endLimit]
    var sql = 'select 1 as direction, Trans_type, Trans_id, transaction_date, Trans_amt,  Trans_ref, trans_desc, From_wallet, To_wallet from mx_wallet_xns where Mobile_no='+uid;
    var sql2='select  2 as direction, Trans_type, Trans_id, transaction_date,  Trans_amt, Trans_ref, trans_desc, From_wallet, To_wallet from mx_wallet_xns where To_wallet='+uid;
    var sql_union=sql+' union '+sql2+' order by transaction_date desc limit ?,?';

    connect.connection()
    .then((conn) => {
        
        conn.query(sql_union,data,(err,results,field) => {
            conn.release();
            if(err){
                // return console.err(err.message);
                console.log("err:",err.message);
                res.json({status:-1})
            }
            else if(results.length == 0){
                console.log("No Transaction History")
                res.json({status:0});
            }
            else{
               // console.log("results:"+JSON.stringify(results));
                res.json({status:1,details:results});
            }
        })

    })
})

router.route('/latest')
.post((req,res) => {

    var uid = req.body.userid;
    console.log("uid:",uid);
    // var sql = "select * from mx_wallet_xns where Mobile_no="+uid;

    var sql = 'select 1 as direction, Trans_type, Trans_id, transaction_date, Trans_amt,  Trans_ref, trans_desc, From_wallet, To_wallet from mx_wallet_xns where Mobile_no='+uid;
    var sql2='select  2 as direction, Trans_type, Trans_id, transaction_date,  Trans_amt, Trans_ref, trans_desc, From_wallet, To_wallet from mx_wallet_xns where To_wallet='+uid;
    var sql_union=sql+' union '+sql2+' order by transaction_date desc limit 5';

    connect.connection()
    .then((conn) => {
        
        conn.query(sql_union,(err,results,field) => {
            conn.release();
            if(err){
                // return console.err(err.message);
                console.log("err:",err.message);
                res.json({status:-1})
            }
            else if(results.length == 0){
                console.log("No Transaction History")
                res.json({status:0});
            }
            else{
               // console.log("results:"+JSON.stringify(results));
                res.json({status:1,details:results});
            }
        })

    })
})
module.exports = router;