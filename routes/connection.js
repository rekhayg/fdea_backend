const express=require('express');
var mysql = require('mysql')
const config = require('../config')
//LocalDB -- change the parameter according to the config of the DB which will be used by the app
  var con = mysql.createPool(
      config.CONNECTION_STRINGS.gpu
  );
let connection = () => {
    return new Promise((resolve, reject) => {
 con.getConnection(function(err,conn) {
    if (err) {
            console.log("MySQL connection error:: " + err.stack);
    }
    else{
        // console.log("Connected to MySQL...");
        console.log("Connected to MySQL..."+ conn.threadId)
        // console.log("con.config.connectionLimit.."+con.config.connectionLimit) // passed in max size of the pool
        // console.log("con._freeConnections.length ..."+con._freeConnections.length ) // number of free connections awaiting use
        // console.log("con._allConnections.length..."+con._allConnections.length) // number of connections currently created, including ones in use
        // console.log("con._acquiringConnections.length.."+con._acquiringConnections.length)   // number of connections in the process of being acquired
        resolve(conn); 
    }
})

})
}


module.exports = {connection:connection};