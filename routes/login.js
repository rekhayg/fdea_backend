const express=require('express');
var connect = require('./connection');
var router = express.Router();
var moment = require('moment')
var cryptify = require('./cryptify')
const async = require('async');

router.route('/')
.post((req,res)=>{
console.log("uid----------",req.body.uid)
    var date = new Date();
    console.log("DATE:"+date);
    var uid = req.body.uid;
    var query1 = `SELECT * FROM mx_wallets where mobile_no='${req.body.uid}';`
    // let query2 = `UPDATE mx_users SET last_login = ? where mobile_number = ?`;
    let data1 = [date, req.body.uid];
    // let attempts = 'select sign_in_attempts from mx_users where mobile_number='+uid;
    let query3 = `UPDATE mx_wallets SET last_loggedin_date = ? where mobile_no = ?`;
    var status;
    connect.connection()
    .then((conn) => {

        async.parallel([
            (callback) => {
                conn.query(query1,  (err, results, fields) => {
                    callback(err,results)
                })
            },
            // (callback) => {
            //     conn.query(query2, data1,(error, results, fields) => {
            //         callback(error,results)
            //     })
            // },
           
            // (callback) => {
            //     conn.query(attempts,  (err, results, fields) => {
            //         callback(err,results)
            //     })
            // },
            
            // (callback) => {
            //     conn.query(query3, data1,(error, results, fields) => {
            //         callback(error,results)
            //     })
            // },
        ], (err, response) => {
           

            if(err){
                conn.release();
                status = -1;
                res.json({status:status})
            }

            else if(response[0].length < 1) {
                conn.release();
                status = 0;
                res.json({
                status:status
                  })
            }
            else{
                console.log("-----Pwd as stored in db",response[0][0].password)
                    let original_pwd = cryptify.decrypt(response[0][0].password)
                    console.log("------original password--", original_pwd)
                    if(req.body.pwd==original_pwd){
                        console.log("Passwords match");

                        
                        // if(parseInt(response[2][0].sign_in_attempts) < 3){
                            if(response[0][0].status=='P')
                            {
                                console.log("--User status-- " + response[0][0].status)
                                status = 3;
                                conn.release();
                                res.json({
                                    status:status
                                })
                            }
                            else{
                                status = 1;
                                var lastlogin = response[0][0].last_loggedin_date;
                                var createddate = response[0][0].created_date;
                                conn.query(query3, data1,(error, results, fields) => {
                                    conn.release();
                                    if(error)
                                        console.error("Error while updating last login")
                                    console.log("Last login updated successfully");
                                                                })
    
                                res.json({
                                    status:status,
                                    details: response[0][0],
                                    last_loggedin_date: lastlogin,
                                    created_date: createddate
                                      })
                            }

                          
                        // }
                        // else{
                        //     status = 2;
                        // }
                       
                    }
                    else{
                        console.log("Password mismatch");
                        status = 0;
                        res.json({
                            status:status
                        })
                        // var count = parseInt(response[2][0].sign_in_attempts);
                        // count++;
                        // console.log("count:",count);
                        // let updatedAttempts = `UPDATE mx_users SET sign_in_attempts = ? where mobile_number = ?`;
                        // let data2 = [count, req.body.uid];
                        // if(count <= 3){
                        //     conn.query(updatedAttempts, data2,(error, results, fields) => {
                        //         if(error)
                        //             console.log("Error while updating signin attempts:",error.message);
                        //         console.log("Signin attempts updated");
                        //     })
                        // }
                        // else{
                        //     console.log("No attempts available");
                        //     status = 2;
                        // }
                       
                    }
            }
        //    res.json({status:status,details:response[0][0]});
        })

    })
    .catch((e)=>{
        console.log("Error--"+e)
    })

})

// router.route('/')
// .post((req,res)=>{
//     // console.log("---Request body-------",req.body)
// console.log("uid----------",req.body.uid)
//     var query1 = `SELECT password FROM mx_wallets where mobile_no='${req.body.uid}';`
//     var query2 = `SELECT * from mx_wallets where mobile_no='${req.body.uid}';`
//     var status;
//     connect.connection()
//     .then((conn) => {
//         conn.query(query2,  (err, results, fields) => {
//             if (err) {
//                 return console.error(err.message);
//             }
//             else{  
//                 console.log("results----------",results.length,"-----",results[0])
//                 if(results.length<1){
//                     console.log("Sending only status.....")
//                     status = 0;
//                     res.json({
//                         status:status
//                     })
//                 }
//                 else{
//                     console.log("-----Pwd as stored in db",results[0].password)
//                     let original_pwd = cryptify.decrypt(results[0].password)
//                     console.log("------original password--", original_pwd)
//                     if(req.body.pwd==original_pwd)
//                     {
//                         console.log("Passwords match.. sending status and details")
//                         status = 1
//                         res.json({
//                             status:status,
//                             details: results[0]
//                         })
//                     }
//                     else{
//                         console.log("Passwords mismatch.. sending only status")
//                         status = 0
//                         res.json({
//                             status:status
//                         })
//                     }

//                 }
//         //    res.json({
//         //        status:status,
//         //        details: results[0]
//         //    })
//          }

//         })

//     })
//     .catch((e)=>{
//         console.log("Error--"+e)
//     })

// })

module.exports=router;