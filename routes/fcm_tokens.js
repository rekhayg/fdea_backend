var express = require('express');
var router = express.Router();
var moment = require('moment')
var connect=require('./connection')
//To fetch the FCM token for a given mobile number
router.route('/')
.post((req,res)=>{
    console.log("---fcm tokens---"+req.headers+"----"+req.headers.uid)
    var status;
    var query1 = `SELECT * from mx_fcm_tokens where mobile_no='${req.headers.uid}';`
    connect.connection()
    .then((conn) => {
        console.log("-----Connection----")
        conn.query(query1,  (err, results, fields) => {
            conn.release();
            if (err) {
                return console.error(err.message);
            }
            else{  
                console.log("results----------",results.length,"-----",results[0])
                if(results.length<1){
                    console.log("No matching mobile number found")
                    status = 0;
                    res.json({
                        status:status
                    })
                }
                else{
                    console.log("____________"+results[0]['token'])
                    status=1;
                    res.json({
                        status:status,
                        token:results[0]['token']
                    })
                }
               
        
         }

        })

    })
    .catch((e)=>{
        console.log("Error--"+e)
    })
})

//Saving token for a mobile number (i.e uid)
router.route('/savetoken')
.post((req,res)=>{
    console.log("Savetoken.........Request.."+req.headers.uid+req.headers.token)
    let query2 = `INSERT INTO mx_fcm_tokens ( mobile_no, token) VALUES 
    ( '${req.headers.uid}', '${req.headers.token}')
    ON DUPLICATE KEY UPDATE 
    token=VALUES(token)
    ;`
    connect.connection()
    .then((conn)=>{
        conn.query(query2,  (err, results, fields) => {
            conn.release();
            if (err) {
                res.json({status:0})
                return console.error(err.message);
            }
            else{  
                res.json({status:1})
         }
        })
    })
    .catch((e)=>{
        console.log("Error"+e)
    })
})
module.exports = router;