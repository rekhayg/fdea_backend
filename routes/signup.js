const express=require('express');
var connect = require('./connection');
var router = express.Router();
var moment = require('moment')
// let connection = mysql.createConnection(config);
const Cryptify = require('./cryptify');
const config= require('../config')
const sgMail = require('@sendgrid/mail');



router.route('/')
    .post((req, res) => {
     console.log('inside signup!')
    var f_name=req.body.first_name;
    var l_name=req.body.last_name;
    var pwd=req.body.pwd;
    var u_type=req.body.u_type;
    var m_no=req.body.m_no;
    var cc=req.body.cc;
    var email=req.body.email
    var status;
    var signup_date = new Date();
    var ccy = req.body.ccy;
    const e_pwd= Cryptify.encrypt(pwd);
    var origin_email = req.body.origin_email
    console.log('encrypted pwd---',e_pwd)
    console.log('decrypt---',Cryptify.decrypt(e_pwd))
    console.log("-------------------------",req.body)
    
    var signup_procedure = 'call POST_REGISTER_ENTRIES(?,?,?,?,?,?,?,?,?,?,?,@cashback_amount,@err_code,@err_desc)';
    var signup_check = 'select @cashback_amount,@err_code,@err_desc';
    var data_signup = [m_no,f_name,l_name,e_pwd,u_type,cc,email,signup_date,m_no,m_no,ccy];

    console.log("data:",data_signup)


    connect.connection()
    .then((conn) => {

    conn.query(signup_procedure, data_signup, (err, results, fields) => {
        // conn.release();
        if (err) {
            conn.release();
            console.log('error in insertion--',err.message);
            res.json({status:-1, cashback_amount: 0})
        }
        else{
            conn.query(signup_check, (err2,result) => {
                conn.release();
                if(err2){
                    console.log('Failed to call cashback procedure')
                    res.json({status:0, cashback_amount: 0})
                }
                else if(result[0]['@err_code'] == 'ERR'){
                    console.log("Falied to register/ fetch cashback....\nCheck error logs in the database.");
                    console.log("Error code:",result[0]['@err_code'],"\nError desc:",result[0]['@err_desc']);
                    res.json({status:0, cashback_amount: 0})
                }
                else{
                    console.log('Inserted successfully...\nCashback amount:',result[0]['@cashback_amount']);
                    res.json({status:1, cashback_amount:result[0]['@cashback_amount']})
                }

            })
            // console.log('inserted!!')
            // console.log('first name in signup',f_name,l_name)
            const msg = {
                to: email,
                from: origin_email,
                html: '<p> </p>',
                templateId: config.SENDGRID_INFINITY_CASH_TEMPLATE,
                // substitutions:
                // {
                //     first_name:f_name,
                //     last_name:l_name
                // }
            };
            sgMail.setApiKey(config.SENDGRID_KEY);
            sgMail.setSubstitutionWrappers('[', ']');
            sgMail.send(msg);

            

            // res.json({status:1})
        }

        });
 
    })
})

router.route('/checkuser')
.post((req,res)=>{
    var email_otp='1234';
    var mobile_otp='1234';
    var u_id=req.body.mobilenumber;
    console.log("==========="+req.body.mobilenumber+typeof req.body.mobilenumber)
    // var date=new Date();
    var u_exists;
    var s_time=moment().format('YYYY-MM-DD HH:mm:ss')
    var query1 = `SELECT * FROM mx_wallets where mobile_no='${u_id}';`
    var query2 = `INSERT INTO mx_otp_details (user_id, email_otp, mobile_otp, start_time) VALUES ('${u_id}', '${email_otp}', '${mobile_otp}', '${s_time}');`
    connect.connection()
        .then((conn)=>{
            conn.query(query1,(err,results,fields)=>{
                if(err){
                    conn.release();
                    res.json({u_exists:-1})
                   console.log('check user err -',err)
                }
                else{
                    console.log('check user result-',results.length)
                    if(results.length==0){
                        u_exists=0;
                        console.log('uexists doesnt exists---------',u_exists)
                        conn.query(query2,  (err, results, fields) => {
                            conn.release();
                            if (err) {
                                return console.error(err.message);
                            }
                            else{
                                console.log('otp cretaed!',u_id)
                                res.json({u_exists:u_exists})
                            }
                        });
                    }
                    else{
                        conn.release();
                        u_exists=1;
                        console.log('uexists exists---------',u_exists)
                        res.json({u_exists:u_exists})
                    }
                }
                    });
                
        })
})

router.route('/otp')
.post((req,res)=>{
    console.log('otp')
    var u_id=req.body.u_id;
    var email_otp=req.body.email_otp;
    var mobile_otp=req.body.mobile_otp;
    var s_time=req.body.s_time;
    var otp_status;
    var query1 = `SELECT mobile_otp FROM mx_otp_details where user_id='${u_id}';`
console.log('mob_no',u_id,'otp---',mobile_otp)
   connect.connection()
   .then((conn) => {
   conn.query(query1,  (err, results, fields) => {
    conn.release();
       if (err) {
           return console.error(err.message);
       }
       else{

        if(results.length){
            console.log("results[0].mobile_otp------------",results[0].mobile_otp,"mobile_otp----------",mobile_otp,"u_id----",u_id)
            if(results[0].mobile_otp==mobile_otp && u_id!=""){
                console.log('otp verified',results[0].mobile_otp);
                res.json({otp_status:1})
            }
            else{
                console.log('wrong otp')
                res.json({otp_status:0})
            }
        }
        else{
            console.log('no  otp')
            res.json({otp_status:0})
        }
    }
       });   
   // connection.end();
  
   })

})
router.route('/setPin')
.post((req,res)=>{
    var u_id=req.body.u_id;
    var pin=req.body.pin;
    console.log("u_id.."+u_id+"----------"+pin)
    var query1 = `update mx_wallets set wallet_pin='${pin}' where mobile_no='${u_id}';`
    connect.connection()
    .then((conn) => {
        
    conn.query(query1,  (err, results, fields) => {
        conn.release();
     if (err) {
            res.json({status:-1})
           return console.error(err.message);
       }
       else{
           console.log('results--',results.affectedRows)
        if(results.affectedRows){
            res.json({status:1})
        }
        else{
            res.json({status:0})
        }
       }
    
    })
})
    // update fdea.mx_wallets set wallet_pin=123 where mobile_no=55555;
})
// router.route('/otp')
// .post((req,res)=>{
//     console.log('otp')
//     var u_id=req.body.u_id;
//     var email_otp=req.body.email_otp;
//     var mobile_otp=req.body.mobile_otp;
//     var s_time=req.body.s_time;

//     var query1 = `SELECT * FROM fdea.mx_otp_details where mobile_otp=10000;`
//    connect.connection()
//    .then((conn) => {
//    conn.query(query1,  (err, results, fields) => {
//        if (err) {
//            return console.error(err.message);
//        }
//        console.log('otp Id:' + results.insertId);
//        });   
//    // connection.end();
//    res.json('otp details added!')
//    })

// })

module.exports=router;