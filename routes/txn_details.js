var express = require('express');
var router = express.Router();
var moment = require('moment')
var con=require('./connection')

router.post('/save_xn_details', function(req, res, next) {
    console.log("-----------req.headers---------",req.headers)
    var date = new Date();

    var trans_desc = 'payment'
    if(req.headers.desc){
        trans_desc = req.headers.desc;
    }
    else{
        trans_desc = 'payment';
    }
    var ccy = req.headers.ccy;
    var procedure_call;
    var procedure_check = 'select @trans_ref,@check1,@err_code,@err_desc';
    var data_procedure;

    switch(req.headers.trans_type){
        case 'FP':
            procedure_call = 'call FUNDING_POS_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.to_wallet_id, req.headers.mno, 
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;
        
        case 'FF':
            procedure_call = 'call FUNDING_FDEA_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.to_wallet_id, req.headers.mno, 
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;

        case 'PA':
            procedure_call = 'call PAYMENT_P2P_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.mno, req.headers.to_wallet_id, 
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;
        
        case 'PP':
            procedure_call = 'call PAYMENT_P2P_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.mno, req.headers.to_wallet_id, 
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;
        
        case 'WP':
            procedure_call = 'call WITHDRAW_POS_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.mno, req.headers.to_wallet_id,
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;
        
        case 'WF':
            procedure_call = 'call WITHDRAW_FDEA_UPDATES(?,?,?,?,?,?,?,@trans_ref,@check1,@check2,@trans_id,@err_code,@err_desc)';
            data_procedure = [req.headers.mno, req.headers.to_wallet_id, 
                                req.headers.trans_type, date, req.headers.trans_amt, 
                                trans_desc, ccy];
            break;

    }




  con.connection()
  .then((conn)=>{


    conn.query(procedure_call, data_procedure, function(err,result) {
        if(err){
            conn.release();
            console.log("Error while updating transaction details:",err.message);
            res.json({
                status:-1, cashback_amount:0
            })
        }
        else{
            conn.query(procedure_check, function(err2,results){
                conn.release();
                if(err2){
                    console.log("Error while fetching transaction details status:",err2.message);
                    res.json({
                        status:0, cashback_amount:0
                    })
                }
                else if(results[0]['@err_code'] == 'ERR'){
                    console.log('error_code',results[0]['@err_code'],'\nError desc:',results[0]['@err_desc'])
                    console.log("Error while updating transaction details. Check error log from the database.");
                    res.json({
                        status:0, cashback_amount:0
                    })
                }
                else{
                    console.log("Transaction details has been updated successfully.");
                    console.log("Transaction reference:",results[0]['@trans_ref']);
                    console.log("Cashback AMount :",results[0]['@check1'])
                    res.json({
                        status: 1,
                        tref: results[0]['@trans_ref'],
                        cashback_amount:results[0]['@check1']
                    })
                }
            })
        }
    })

    
  })
  
});


module.exports = router;