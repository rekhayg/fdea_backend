var express = require('express');
var router = express.Router();
var moment = require('moment')
var con=require('./connection')

router.route('/')
.post((req,res) => {

    var updateLang = 'update mx_wallets set preferred_lang = ? where mobile_no = ?'
    var data = [req.body.lang, req.body.userid];
    console.log('data:',data)

    con.connection()
    .then((conn) => {

        conn.query(updateLang,data,function(err, result){
            conn.release();
            if(err){
                console.log('Error while updating language');
                res.json({status:0});
            }
            else{
                console.log("Preferred language updated successfully");
                res.json({status:1})
            }
        })

    })

})

module.exports = router;