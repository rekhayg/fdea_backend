const config = require('./config');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var http=require('http')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
var mysql = require('mysql');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/txn_details',require('./routes/txn_details'))
app.use('/fetchPassword',require('./routes/FetchPassword'))
app.use('/login',require('./routes/login'))
app.use('/signup',require('./routes/signup'))
app.use('/notify',require('./routes/notification'))
app.use('/fdea_account',require('./routes/fdea_account'))
app.use('/fcm_tokens',require('./routes/fcm_tokens'))
app.use('/params',require('./routes/params'))
app.use('/transactionHistory',require('./routes/TransactionHistory'))

app.use('/userprofile',require('./routes/userprofile'))
app.use('/getWalletBalance',require('./routes/GetWalletBalance'));
app.use('/verifyNumber',require('./routes/verifyNumber'))
app.use('/promotion',require('./routes/promotion'))
app.use('/updateLanguage',require('./routes/UpdateLanguage'))
app.use('/getPromotions',require('./routes/GetPromotions'));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// var con = mysql.createPool({
//   // var con = mysql.createConnection({
//   connectionLimit : 100,
//   port     :  3306,
//   host     : "localhost",
//   user     : "root",
//   password : "admin123",
//   database : "fdea_users",
// });

// con.getConnection(function(err) {
//   if (err) {
//         console.log("MySQL connection error:: " + err.stack);
//   }
// else
//   console.log("Connected to MySQL...");
//   // var sql = `INSERT INTO mx_user_profile(First_name,Last_name,Email,Mobile_number,Address_line_1,Address_line_2,City,PIN,State,Country)
//   // VALUES ('Mahalakshmi','Lakshmi','maha@gmail.com','999999999','abc','cde','chennai','123','TN','INDIA')`
//   // con.query(sql, function (err, result) {
//   //   if (err) throw err;
//   //   console.log("1 record inserted");
//   // });
// })



// var con = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "admin123"
// });

// con.connect(function(err,res) {
//   if (err) throw err;
//   console.log("Connected!",res);
// });
// var options = {
//   key: fs.readFileSync("./ssl/botdev_clouddelphi.com.key"),
//  cert: fs.readFileSync("./ssl/botdev_clouddelphi.com.crt"),
//  ca: [
//       fs.readFileSync("./ssl/botdev_clouddelphi_com.ca-bundle")
//     ]  
// };
var port= config.PORT
http.createServer(app).listen(port,()=>{
// console.log('Microservice listening at '+config.PORT)
console.log('App listening at '+port)
// logger.info('Microservice listening at '+config.PORT)
})
// var server = app.listen(3000, function () {
//   var host = server.address().address
//   var port = server.address().port
//   console.log("App listening at http://%s:%s", host, port)
// })


// module.exports = app;
