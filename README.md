# fdea_backend

# config.js file
Change the values for the connection strings (config.CONNECTION_STRINGS.local) based on the Local DB , when working locally. 

# connection.js file 
Use config.CONNECTION_STRINGS.local as the parameter to create pool, when working locally.
When the app has to connect to the DB on the GPU, use config.CONNECTION_STRINGS.gpu instead.

#Connection to the DB on the GPU 
To connect to the DB on the GPU from the server app, an SSH tunnel has to be created. 